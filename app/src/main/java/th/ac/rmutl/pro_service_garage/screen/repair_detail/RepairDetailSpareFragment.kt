package th.ac.rmutl.pro_service_garage.screen.repair_detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.Spare


/**
 * A simple [Fragment] subclass.
 */
class RepairDetailSpareFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater!!.inflate(R.layout.fragment_repair_detail_spare, container, false)
        val spareRecycler = root.findViewById<RecyclerView>(R.id.spare_recycler_view)
        spareRecycler.setHasFixedSize(true)
        val mLayoutManager  = LinearLayoutManager(context)
        spareRecycler.setLayoutManager(mLayoutManager)

        val spareList = arguments?.getSerializable("spare")

        val spareListAdapter = SpareListAdapter(spareList as ArrayList<Spare>)
        spareRecycler.setAdapter(spareListAdapter)

        return root
    }

    companion object {
        fun newInstance(spare: ArrayList<Spare>): RepairDetailSpareFragment {
            val args = Bundle()
            args.putSerializable("spare", spare)
            val fragment = RepairDetailSpareFragment()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor

package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/12/18.
 */

data class Account(
        val user_id: Int,
        val username: String,
        val first_name: String,
        val last_name: String,
        val phone_no: String,
        val address: String,
        override val res_code: String,
        override val res_desc: String,
        override val token: String
) : Response
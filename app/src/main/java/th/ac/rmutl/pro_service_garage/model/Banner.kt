package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class Banner(
        val title: String,
        val message: String,
        val image: String
)
package th.ac.rmutl.pro_service_garage.prefer

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by digio-macair13 on 2/21/18.
 */
class NotificationTokenPreferences(context: Context) {
    private var stringPreference: SharedPreferences? = null
    val PREFS_FILENAME = "th.ac.rmutl.pro_service_garage.prefs"
    val NOTIFICATION_TOKEN = "notificationToken"
    init {
        stringPreference = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
    }
    val isSet: Boolean
        get() = stringPreference!!.contains(NOTIFICATION_TOKEN)

    fun get(): String {
        return stringPreference!!.getString(NOTIFICATION_TOKEN, "")
    }

    fun set(value: String) {
        stringPreference!!.edit().putString(NOTIFICATION_TOKEN, value).apply()
    }

    fun delete() {
        stringPreference!!.edit().remove(NOTIFICATION_TOKEN).apply()
    }
}
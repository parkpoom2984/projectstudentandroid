package th.ac.rmutl.pro_service_garage.screen.repair

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.Car

class CarListAdapter(private val listCar: ArrayList<Car>,val listener: CarClick) : RecyclerView.Adapter<CarListAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val card_view = LayoutInflater.from(parent.context).inflate(R.layout.card_car, null)
        val viewHolder = ViewHolder(card_view)
        card_view.setOnClickListener(this)
        viewHolder.cardView!!.setOnClickListener(this)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val car = listCar[position]
        holder.cardView.tag = car
        holder!!.carTextView.setText(car.licensePlate + " " + car.provincePlate)
    }

    override fun getItemCount(): Int {
        return listCar.size
    }

    override fun onClick(view: View) {
        if (view.id == R.id.card_view) {
            listener.onClick(view.tag as Car)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var carTextView = itemView.findViewById<TextView>(R.id.car_textview)
        var cardView = itemView.findViewById<CardView>(R.id.card_view)
    }

    interface CarClick {
        fun onClick(car: Car)
    }
}
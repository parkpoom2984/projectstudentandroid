package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 3/31/18.
 */
data class AddCarRequest(
        val userId: Int,
        val licensePlate: String,
        val provincePlate: String,
        val color: String,
        val brand: String
)
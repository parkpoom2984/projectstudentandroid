package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class RepairRequest(
        val latitude: String,
        val longtitude: String,
        val userId: Int,
        val licensePlate: String,
        val provincePlate: String,
        val color: String,
        val issue: String,
        val trailerType: String

)
package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */
interface Response {
    val res_code: String
    val res_desc: String
    val token: String
}
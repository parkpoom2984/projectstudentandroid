package th.ac.rmutl.pro_service_garage.utils

import com.stepstone.apprating.C



/**
 * Created by digio-macair13 on 3/31/18.
 */
class ConvertStatusRepair {

    fun convert(status: String) : String {
        when (status) {
            "Unappove"-> return "ส่งคำร้องการซ่อม"
            "Repair" -> return "แจ้งเตือนการซ่อม"
            "Accept" -> return "ตอบรับการแจ้งซ่อม"
            "Quotations" -> return "ออกใบเสนอราคา"
            "Appove" -> return "ชำระเงินเรียบร้อยแล้ว"
            "Invoice" -> return "ออกใบเรียกเก็บเงิน"
        }
        return ""
    }
}
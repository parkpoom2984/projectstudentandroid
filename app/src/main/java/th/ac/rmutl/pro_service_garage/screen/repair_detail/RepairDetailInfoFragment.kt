package th.ac.rmutl.pro_service_garage.screen.repair_detail


import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.stepstone.apprating.AppRatingDialog
import com.stepstone.apprating.listener.RatingDialogListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.*
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus
import th.ac.rmutl.pro_service_garage.utils.ConvertStatusRepair
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class RepairDetailInfoFragment : Fragment() {
    private var progressDialog: ProgressDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater!!.inflate(R.layout.fragment_repair_detail_info, container, false)
        val feedBackButton = root.findViewById<TextView>(R.id.button_feed_back)
        val employeeActiveButton = root.findViewById<TextView>(R.id.button_employee_active)

        val statusTextView = root.findViewById<TextView>(R.id.status_textview)
        val carTextView = root.findViewById<TextView>(R.id.car_textview)
        val issueTextView = root.findViewById<TextView>(R.id.issue_textview)
        val totalTextView = root.findViewById<TextView>(R.id.total_textview)
        val discountTextView = root.findViewById<TextView>(R.id.discount_textview)
        val sumSpareTextView = root.findViewById<TextView>(R.id.sumSpare_textview)
        val employeeNameTextView = root.findViewById<TextView>(R.id.employee_name_textview)
        val employeePhoneTextView = root.findViewById<TextView>(R.id.employee_phone_textview)

        val repair = arguments?.getSerializable("repair") as Repair
        statusTextView.setText(ConvertStatusRepair().convert(repair.status))
        carTextView.setText(repair.licensePlate + " " + repair.provincePlate)
        issueTextView.setText(repair.issue)
        totalTextView.setText(repair.total.toString() + " บาท")
        discountTextView.setText(repair.sumDiscount.toString()+ " บาท ("+repair.discount.toString()+"%)")
        sumSpareTextView.setText(repair.sumSpare.toString()+ " บาท")
        employeeNameTextView.setText(repair.firstName + " "+ repair.lastName)
        employeePhoneTextView.setText(repair.phoneNo)
        feedBackButton.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                showDialog()
            }

        })
        employeeActiveButton.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                progressDialog = ProgressDialog(context)
                progressDialog?.setMessage("โหลดข้อมูล")
                progressDialog?.setCancelable(false)
                progressDialog?.setCanceledOnTouchOutside(false)
                val api = Api()
                progressDialog?.show()
                api.employeeActiveRequest(EmployeeActiveRequest(repair!!.repairId))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ emptyResponse -> checkResponse(emptyResponse) },
                                { e -> println(e) },
                                {      println("onComplete") })
            }

        })
        return root
    }


    private fun showDialog() {
        AppRatingDialog.Builder()
                .setPositiveButtonText("ยืนยัน")
                .setNegativeButtonText("ยกเลิก")
                .setNoteDescriptions(Arrays.asList("แย่มาก", "ไม่ดี", "ปานกลาง", "ดี", "ดีมาก"))
                .setDefaultRating(2)
                .setTitle("คะแนนการซ่อม")
                .setDescription("โปรคเลือกระดับดาวตามความคิดเห็นของคุณ")
                .setStarColor(R.color.psg_yellow)
                .setNoteDescriptionTextColor(R.color.psg_gray)
                .setTitleTextColor(R.color.psg_gray_dark)
                .setDescriptionTextColor(R.color.psg_gray_dark)
                .setHint("โปรคเขียนคำติชมตรงนี้ ...")
                .setHintTextColor(R.color.colorWhite)
                .setCommentTextColor(R.color.psg_gray_dark)
                .setCommentBackgroundColor(R.color.psg_gray)
//                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .create(activity!!)
                .show()
    }

    fun toastMessage(message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(emptyResponse : EmptyResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(emptyResponse.res_code)){
            showAlertMessage()
        }else{
            toastMessage(emptyResponse.res_desc)
        }
    }

    fun showAlertMessage(){
        AlertDialog.Builder(context!!)
                .setTitle("แจ้งเตือน")
                .setMessage("ส่งข้อมูลไปยังระบบเรียบร้อยแล้ว !!!")
                .setCancelable(false)
                .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        dialog.cancel()
                    }
                }).show()
    }

    companion object {
        fun newInstance(repair: Repair): RepairDetailInfoFragment {
            val args = Bundle()
            args.putSerializable("repair", repair)
            val fragment = RepairDetailInfoFragment()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor

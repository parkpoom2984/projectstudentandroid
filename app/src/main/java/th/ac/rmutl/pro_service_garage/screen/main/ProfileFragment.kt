package th.ac.rmutl.pro_service_garage.screen.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater!!.inflate(R.layout.fragment_profile, container, false)
        val accountPreference = AccountPreferences(context!!)
        val firstName = accountPreference.getFirstName()
        val lastName = accountPreference.getLastName()
        val phoneNo = accountPreference.getPhoneNo()
        val address = accountPreference.getAddress()

        var nameTextView = root.findViewById<TextView>(R.id.name)
        var phoneNoTextView = root.findViewById<TextView>(R.id.phoneNo)
        var addressTextView = root.findViewById<TextView>(R.id.address)

        nameTextView.setText(firstName + " " + lastName)
        phoneNoTextView.setText(phoneNo)
        addressTextView.setText(address)

        return root
    }

}// Required empty public constructor

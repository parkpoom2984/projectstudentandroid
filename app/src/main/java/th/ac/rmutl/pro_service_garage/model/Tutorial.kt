package th.ac.rmutl.pro_service_garage.model

import th.ac.rmutl.pro_service_garage.R

/**
 * Created by digio-macair13 on 2/23/18.
 */
enum class Tutorial private constructor(val titleResId: String, val layoutResId: Int) {
    RED("red", R.layout.fragment_tutorial_first),
    BLUE("buld", R.layout.fragment_tutorial_second),
    GREEN("green", R.layout.fragment_tutorial_third)
}
package th.ac.rmutl.pro_service_garage.screen.tutorial

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import th.ac.rmutl.pro_service_garage.R

/**
 * A simple [Fragment] subclass.
 */
class TutorialFirstFragment() : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater!!.inflate(R.layout.fragment_tutorial_first, container, false)
        val titleTextView = rootView.findViewById<TextView>(R.id.title_text_view)
        val messageTextView = rootView.findViewById<TextView>(R.id.message_text_view)
        val bannerImageView = rootView.findViewById<ImageView>(R.id.banner_image_view)

        val title = arguments?.getString("title")
        val message = arguments?.getString("message")
        val image = arguments?.getString("image")
        // val decodedBytes = Base64.decode(image, Base64.DEFAULT)
        Glide.with(this).load(image).into(bannerImageView)
        titleTextView.setText(title)
        messageTextView.setText(message)
        return rootView
    }

    companion object {
        fun newInstance(title: String, message: String, image: String): TutorialFirstFragment {
            val args = Bundle()
            args.putString("title", title)
            args.putString("message", message)
            args.putString("image", image)
            val fragment = TutorialFirstFragment()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
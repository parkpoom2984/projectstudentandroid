package th.ac.rmutl.pro_service_garage.screen.tutorial

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_tutorial.*
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.BannerResponse
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus


class TutorialActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
        var api = Api()
        progressDialog?.show()
        api.getBannerRequest()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ account -> checkResponse(account) },
                        { e -> println(e) },
                        {      println("onComplete") })
    }

    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(bannerResponse: BannerResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(bannerResponse.res_code)){
            viewPager.adapter = FragmentViewPagerAdapter(supportFragmentManager, bannerResponse.list_banner)
            setupViewPager(bannerResponse.list_banner.size)
        }else{
            toastMessage(bannerResponse.res_desc)
        }
    }

    fun setupViewPager(size : Int){
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                println("sammy_current_position: " + position)

                if(position == size){
                    skip_button.visibility = View.GONE
                }else{
                    skip_button.visibility = View.VISIBLE
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        skip_button.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                viewPager.setCurrentItem(size)
            }
        })
    }
}

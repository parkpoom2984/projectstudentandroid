package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class ScheduleResponse(
        val list_schedule: ArrayList<Schedule>,
        override val res_code: String,
        override val res_desc: String,
        override val token: String
): Response
package th.ac.rmutl.pro_service_garage.screen.main

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.Repair
import th.ac.rmutl.pro_service_garage.utils.ConvertStatusRepair

class HistoryListAdapter(private val listHistory: ArrayList<Repair>,val listener: HistoryClick) : RecyclerView.Adapter<HistoryListAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val card_view = LayoutInflater.from(parent.context).inflate(R.layout.card_history, null)
        val viewHolder = ViewHolder(card_view)
        card_view.setOnClickListener(this)
        viewHolder.cardView!!.setOnClickListener(this)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repair = listHistory[position]
        holder!!.refIdTextView.setText("ref-" + repair.repairId)
        holder!!.carTextView.setText(repair.licensePlate + " " + repair.provincePlate)
        holder!!.statusTextView.setText(ConvertStatusRepair().convert(repair.status))
        holder.cardView.tag = repair
    }

    override fun getItemCount(): Int {
        return listHistory.size
    }

    override fun onClick(view: View) {
        if (view.id == R.id.card_view) {
            listener.onClick(view.tag as Repair)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var refIdTextView = itemView.findViewById<TextView>(R.id.ref_id_textview)
        var carTextView = itemView.findViewById<TextView>(R.id.car_textview)
        var cardView = itemView.findViewById<CardView>(R.id.card_view)
        var statusTextView = itemView.findViewById<TextView>(R.id.status_textview)
    }

    interface HistoryClick {
        fun onClick(repair: Repair)
    }
}


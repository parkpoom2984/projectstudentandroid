package th.ac.rmutl.pro_service_garage.screen.register

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.model.Account
import th.ac.rmutl.pro_service_garage.model.RegisterRequest
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register_profile.*
import th.ac.rmutl.pro_service_garage.prefer.NotificationTokenPreferences
import th.ac.rmutl.pro_service_garage.utils.ApiStatus

class RegisterProfileActivity : AppCompatActivity() {
    private var accountPreference: AccountPreferences? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_profile)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)

        val notificationTokenPreference = NotificationTokenPreferences(this)
        button_register.setOnClickListener {
            val api = Api()
            progressDialog?.show()
            api.registerRequest(RegisterRequest(input_username.text.toString(), input_password.text.toString(), input_firstname.text.toString(), input_lastname.text.toString(), input_phoneNo.text.toString(),notificationTokenPreference.get()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ account -> checkResponse(account) },
                            { e -> println(e) },
                            {      println("onComplete") })
        }
    }


    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    fun goToRegisterAddressActivity(){
        var intent = Intent(this, RegisterAddressActivity::class.java)
        startActivity(intent)
    }

    fun checkResponse(account : Account){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(account.res_code)){
            accountPreference = AccountPreferences(this)
            accountPreference!!.set(account.user_id.toString(),account.username,account.first_name,account.last_name,account.phone_no,"")
            goToRegisterAddressActivity()
        }else{
            toastMessage(account.res_desc)
        }
    }
}

package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */
import java.io.*

data class Repair (
        val repairId: Int,
        val status: String,
        val issue: String,
        val licensePlate: String,
        val provincePlate: String,
        val sumSpare: Double,
        val discount: Double,
        val sumDiscount: Double,
        val total: Double,
        val firstName: String,
        val lastName: String,
        val phoneNo: String,
        val list_spare: ArrayList<Spare>
) : Serializable
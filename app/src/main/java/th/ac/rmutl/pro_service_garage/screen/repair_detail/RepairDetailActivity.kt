package th.ac.rmutl.pro_service_garage.screen.repair_detail

import android.app.ProgressDialog
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.stepstone.apprating.listener.RatingDialogListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import th.ac.rmutl.pro_service_garage.R
import kotlinx.android.synthetic.main.activity_repair_detail.*
import th.ac.rmutl.pro_service_garage.model.*
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus


class RepairDetailActivity : AppCompatActivity(), RatingDialogListener {
    private var progressDialog: ProgressDialog? = null
    private var repair: Repair? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repair_detail)

        val intent = intent
        repair = intent.getSerializableExtra("repair") as Repair
//        var list : ArrayList<Spare> = ArrayList<Spare>()
//        list.add(Spare("wheel",2000,1))
//        val repair = Repair(26,"test","test","test","test", list)

        initInstancesDrawer("Ref-"+repair!!.repairId)

        tabLayout.addTab(tabLayout.newTab().setText("รายละเอียด"))
        tabLayout.addTab(tabLayout.newTab().setText("อะไหล่"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = RepairDetailPagerAdapter(supportFragmentManager, 2, repair!!)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    fun initInstancesDrawer(title: String) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        collapsing_toolbar_layout.setTitle(title)
    }


    override fun onNegativeButtonClicked() {

    }

    override fun onNeutralButtonClicked() {

    }

    override fun onPositiveButtonClicked(rate: Int, comment: String) {
        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
        val api = Api()
        progressDialog?.show()
        api.feedbackRequest(FeedbackRequest(repair!!.repairId,rate,comment))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ emptyResponse -> checkResponse(emptyResponse) },
                        { e -> println(e) },
                        {      println("onComplete") })
    }

    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(emptyResponse : EmptyResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(emptyResponse.res_code)){
            showAlertMessage()
        }else{
            toastMessage(emptyResponse.res_desc)
        }
    }

    fun showAlertMessage(){
        AlertDialog.Builder(this)
                .setTitle("แจ้งเตือน")
                .setMessage("ความคิดเห็นของคุณ ส่งไปยังระบบเรียบร้อยแล้ว !!!")
                .setCancelable(false)
                .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        dialog.cancel()
                    }
                }).show()
    }

}

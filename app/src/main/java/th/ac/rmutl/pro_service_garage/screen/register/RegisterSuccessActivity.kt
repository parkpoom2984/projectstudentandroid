package th.ac.rmutl.pro_service_garage.screen.register

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import th.ac.rmutl.pro_service_garage.R
import kotlinx.android.synthetic.main.activity_register_success.*
import th.ac.rmutl.pro_service_garage.screen.main.MainActivity

class RegisterSuccessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_success)

        button_main.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                goToMainActivity()
            }
        })
    }

    fun goToMainActivity(){
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}

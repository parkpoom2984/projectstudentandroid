package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class RegisterRequest(
        val username: String,
        val password: String,
        val first_name: String,
        val last_name: String,
        val phone_no: String,
        val notification_token: String
)
package th.ac.rmutl.pro_service_garage.utils

/**
 * Created by digio-macair13 on 2/21/18.
 */
class ApiStatus {
    fun isSuccess(res_code: String) = res_code.equals("0000")
}
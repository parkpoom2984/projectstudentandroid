package th.ac.rmutl.pro_service_garage.screen.repair

import android.app.Dialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.text.InputType
import android.view.View
import android.widget.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_repair.*
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.*
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import th.ac.rmutl.pro_service_garage.screen.main.MainActivity
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus
import android.widget.RadioButton




class RepairActivity : AppCompatActivity() , CarListAdapter.CarClick{

    private var progressDialog: ProgressDialog? = null
    private var targetLatitude = 0.0
    private var targetLongitude = 0.0

    override fun onClick(car: Car) {
        showAlert(car)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repair)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val intent = intent
        targetLatitude = intent.getDoubleExtra("targetLatitude",0.0)
        targetLongitude = intent.getDoubleExtra("targetLongitude",0.0)

        car_recycler_view!!.setHasFixedSize(true)
        val mLayoutManager  = LinearLayoutManager(this)
        car_recycler_view!!.setLayoutManager(mLayoutManager)

        val accountPreference = AccountPreferences(this)
        val userId = accountPreference.getUserId().toInt()

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
        val api = Api()
        progressDialog?.show()
        api.listCarRequest(UserRequest(userId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ listCarResponse -> checkResponseListCar(listCarResponse) },
                        { e -> println(e) },
                        {      println("onComplete") })


        fab.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                goToAddCarActivity()
            }
        })
//        var list : ArrayList<Car> = ArrayList<Car>()
//        list.add(Car("1234","test","blue"))
//        list.add(Car("4321","test","red"))
//        carListAdapter = CarListAdapter(list,this)
//        car_recycler_view!!.setAdapter(carListAdapter)

    }

    fun checkResponseListCar(listCarResponse: ListCarResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(listCarResponse.res_code)){
            val carListAdapter = CarListAdapter(listCarResponse.list_car, this)
            car_recycler_view.setAdapter(carListAdapter)
        }else{
            toastMessage(listCarResponse.res_desc)
        }
    }

    fun checkResponseRepair(response : EmptyResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(response.res_code)){
            showAlertMessage()
        }else{
            toastMessage(response.res_desc)
        }
    }

    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun goToMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun goToAddCarActivity(){
        val intent = Intent(this, AddCarActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun showAlertMessage(){
        AlertDialog.Builder(this)
                .setTitle("แจ้งเตือน")
                .setMessage("การแจ้งซ่อมของคุณ ส่งไปยังระบบเรียบร้อบแล้ว !!!")
                .setCancelable(false)
                .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        goToMainActivity()
                    }
                }).show()
    }


    fun showInputIssue(car: Car){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("อาการรถ")

        // Set up the input
        val input = EditText(this)
        input.setPadding(48,0,48,0)
        input.setBackgroundResource(android.R.color.transparent)

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.inputType = InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE
        input.setLines(3)
        builder.setView(input)

        // Set up the buttons
        builder.setPositiveButton("ตกลง") { dialog, which ->

        }

        builder.setNegativeButton("ยกเลิก") { dialog, which -> dialog.cancel() }

        builder.show()
    }


    fun showAlert(car: Car){
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.setContentView(R.layout.dialog_repair)

        val sendButton = dialog.findViewById<TextView>(R.id.send_button)
        val issueEditText = dialog.findViewById<EditText>(R.id.issue_editText)
        val radioGroup = dialog.findViewById<RadioGroup>(R.id.radio_group)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)

        val accountPreference = AccountPreferences(this)
        val userId = accountPreference.getUserId().toInt()
        // if button is clicked, close the custom dialog
        sendButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                dialog.dismiss()
                val selectedId = radioGroup.checkedRadioButtonId

                // find the radiobutton by returned id
                var type = ""
                if(selectedId == -1){
                    type = "ไม่มี"
                }else{
                    val radioButton = dialog.findViewById<RadioButton>(selectedId)
                    type = radioButton.text.toString()
                }
                val api = Api()
                progressDialog?.show()
                api.repairRequest(RepairRequest(targetLatitude.toString(),targetLongitude.toString(),userId,car.licensePlate,car.provincePlate,car.color, issueEditText.text.toString(), type))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response -> checkResponseRepair(response) },
                                { e -> println(e) },
                                {      println("onComplete") })
            }
        })
        dialog.show()
    }
}

package th.ac.rmutl.pro_service_garage.screen.repair

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_car.*
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.AddCarRequest
import th.ac.rmutl.pro_service_garage.model.Car
import th.ac.rmutl.pro_service_garage.model.EmptyResponse
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus


class AddCarActivity : AppCompatActivity() {

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_car)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
        val accountPreference = AccountPreferences(this)
        val userId = accountPreference.getUserId().toInt()

        button_add_car.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val api = Api()
                progressDialog?.show()
                api.addCarRequest(AddCarRequest(userId, input_licensePlate.text.toString(),input_provincePlate.text.toString(),input_color.text.toString(), input_brand.text.toString()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response -> checkResponse(response) },
                                { e -> println(e) },
                                {      println("onComplete") })
            }
        })

    }

    fun goToRepairActivity(){
        val intent = Intent(this, RepairActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun checkResponse(response : EmptyResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(response.res_code)){
            showAlertMessage()
        }else{
            toastMessage(response.res_desc)
        }
    }

    fun showAlertMessage(){
        AlertDialog.Builder(this)
                .setTitle("แจ้งเตือน")
                .setMessage("เพื่มรถเรียบร้อยแล้ว !!!")
                .setCancelable(false)
                .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        goToRepairActivity()
                    }
                }).show()
    }

    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

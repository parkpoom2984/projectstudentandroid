package th.ac.rmutl.pro_service_garage.screen.main

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.*
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus

/**
 * A simple [Fragment] subclass.
 */
class ScheduleFragment : Fragment() {
    private var progressDialog: ProgressDialog? = null
    var scheduleRecycler: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater!!.inflate(R.layout.fragment_schedule, container, false)
        scheduleRecycler = root.findViewById<RecyclerView>(R.id.schedule_recycler_view)
        scheduleRecycler!!.setHasFixedSize(true)
        val mLayoutManager  = LinearLayoutManager(activity)
        scheduleRecycler!!.setLayoutManager(mLayoutManager)

        val accountPreference = AccountPreferences(context!!)
        val userId = accountPreference.getUserId().toInt()

        progressDialog = ProgressDialog(context)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
        val api = Api()
        progressDialog?.show()
        api.scheduleRequest(UserRequest(userId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ scheduleResponse -> checkResponse(scheduleResponse) },
                        { e -> println(e) },
                        {      println("onComplete") })

        return root
    }

    fun toastMessage(message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(scheduleResponse: ScheduleResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(scheduleResponse.res_code)){
            val scheduleListAdapter = ScheduleListAdapter(scheduleResponse.list_schedule)
            scheduleRecycler!!.setAdapter(scheduleListAdapter)
        }else{
            toastMessage(scheduleResponse.res_desc)
        }
    }

}// Required empty public constructor

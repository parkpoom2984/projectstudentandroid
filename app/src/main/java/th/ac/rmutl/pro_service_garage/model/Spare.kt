package th.ac.rmutl.pro_service_garage.model

import java.io.Serializable

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class Spare (
        val spareName: String,
        val price: Int,
        val total: Int
): Serializable
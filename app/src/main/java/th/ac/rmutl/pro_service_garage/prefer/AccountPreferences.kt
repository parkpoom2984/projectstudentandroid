package th.ac.rmutl.pro_service_garage.prefer

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by digio-macair13 on 2/21/18.
 */
class AccountPreferences(context: Context) {
    private var stringPreference: SharedPreferences? = null
    val PREFS_FILENAME = "th.ac.rmutl.pro_service_garage.prefs"
    val USERNAME = "username"
    val FIRST_NAME = "first_name"
    val LAST_NAME = "last_name"
    val PHONE_NO = "phone_no"
    val USER_ID = "user_id"
    val ADDRESS = "address"
    init {
        stringPreference = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
    }

    fun getUsername(): String {
        return stringPreference!!.getString(USERNAME, "")
    }

    fun getFirstName(): String {
        return stringPreference!!.getString(FIRST_NAME, "")
    }

    fun getLastName(): String {
        return stringPreference!!.getString(LAST_NAME, "")
    }

    fun getPhoneNo(): String {
        return stringPreference!!.getString(PHONE_NO, "")
    }

    fun getUserId(): String {
        return stringPreference!!.getString(USER_ID, "")
    }

    fun getAddress(): String {
        return stringPreference!!.getString(ADDRESS, "")
    }

    fun set(user_id: String,username: String,first_name: String,last_name: String,phone_no: String, address: String) {
        stringPreference!!.edit().putString(USER_ID, user_id).apply()
        stringPreference!!.edit().putString(USERNAME, username).apply()
        stringPreference!!.edit().putString(FIRST_NAME, first_name).apply()
        stringPreference!!.edit().putString(LAST_NAME, last_name).apply()
        stringPreference!!.edit().putString(PHONE_NO, phone_no).apply()
        stringPreference!!.edit().putString(ADDRESS, address).apply()
    }

    fun setAddress(address: String) {
        stringPreference!!.edit().putString(ADDRESS, address).apply()
    }

    fun delete() {
        stringPreference!!.edit().remove(USERNAME).apply()
        stringPreference!!.edit().remove(FIRST_NAME).apply()
        stringPreference!!.edit().remove(LAST_NAME).apply()
        stringPreference!!.edit().remove(PHONE_NO).apply()
        stringPreference!!.edit().remove(ADDRESS).apply()
    }
}
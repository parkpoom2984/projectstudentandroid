package th.ac.rmutl.pro_service_garage.screen.repair_detail

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import th.ac.rmutl.pro_service_garage.model.Repair
import java.io.Serializable

/**
 * Created by digio-macair13 on 2/28/18.
 */
class RepairDetailPagerAdapter(fm: FragmentManager, internal var mNumOfTabs: Int, var repair: Repair) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> {
                return RepairDetailInfoFragment.newInstance(repair)
            }
            1 -> {
                return RepairDetailSpareFragment.newInstance(repair.list_spare)
            }
            else -> return null
        }
    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}
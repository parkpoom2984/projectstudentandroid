package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class LoginRequest(
        val username: String,
        val password: String,
        val notification_token: String
)
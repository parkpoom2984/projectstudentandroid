package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 3/4/18.
 */

data class Schedule(
        val pickDate: String,
        val licensePlate: String,
        val provincePlate: String
)
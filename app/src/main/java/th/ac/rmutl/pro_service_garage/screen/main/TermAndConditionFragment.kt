package th.ac.rmutl.pro_service_garage.screen.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView

import th.ac.rmutl.pro_service_garage.R


/**
 * A simple [Fragment] subclass.
 */
class TermAndConditionFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater!!.inflate(R.layout.fragment_term_and_condition, container, false)
        val webView : WebView = rootView.findViewById<WebView>(R.id.webView)
        webView.loadUrl("https://proservicegarage.doiloirevenue.com/TermAndCondition.aspx")
        return rootView
    }

}// Required empty public constructor

package th.ac.rmutl.pro_service_garage.screen.main


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import th.ac.rmutl.pro_service_garage.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.MapView
import th.ac.rmutl.pro_service_garage.screen.repair.AddCarActivity
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import th.ac.rmutl.pro_service_garage.screen.repair.RepairActivity

/**
 * A simple [Fragment] subclass.
 */
class RepairFragment : Fragment() {
    private var googleMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var targetLatitude: Double? = null
    private var targetLongitude: Double? = null
    private val REQUEST_PERMISSION_FINE_LOCATION = 1
    private var marker: Marker? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_repair, container, false)
        mapView = rootView.findViewById<MapView>(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(activity?.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView?.getMapAsync(object : OnMapReadyCallback {
            override fun onMapReady(mMap: GoogleMap) {
                googleMap = mMap

                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // For showing a move to my location button
                    requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_FINE_LOCATION)
                }else{
                    setupLocation()
                }


                googleMap!!.setOnCameraChangeListener { cameraPosition ->
                    // Cleaning all the markers.
                    val options = MarkerOptions()
                            .position(cameraPosition.target)

                    if (marker != null) {
                        marker!!.remove()
                    }

                    updateTargetLocation(cameraPosition.target.latitude, cameraPosition.target.longitude)
                    marker = mMap.addMarker(options)
                }
            }
        })

        val buttonRepair = rootView.findViewById<TextView>(R.id.button_repair)

        buttonRepair.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                goToRepairActivity()
            }
        })

        return rootView
    }


    @SuppressLint("MissingPermission")
    fun setupLocation() {
        googleMap!!.setMyLocationEnabled(true)

        val myLocationChangeListener = object : GoogleMap.OnMyLocationChangeListener {
            override fun onMyLocationChange(location: Location) {
                val loc = LatLng(location.getLatitude(), location.getLongitude())
                if (marker != null) {
                    marker!!.remove()
                }
                updateTargetLocation(location.getLatitude(), location.getLongitude())
                marker = googleMap!!.addMarker(MarkerOptions().position(loc))
                googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
        }

        googleMap!!.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    fun updateTargetLocation(latitude: Double, longitude: Double){
        this.targetLatitude = latitude
        this.targetLongitude = longitude
    }

    fun goToRepairActivity(){
        val intent = Intent(context, RepairActivity::class.java)
        intent.putExtra("targetLatitude", targetLatitude)
        intent.putExtra("targetLongitude",targetLongitude)
        startActivity(intent)
    }

    private fun requestPermission(permissionName: String, permissionRequestCode: Int) {
        ActivityCompat.requestPermissions(activity!!,
                arrayOf(permissionName), permissionRequestCode)
    }

}// Required empty public constructor

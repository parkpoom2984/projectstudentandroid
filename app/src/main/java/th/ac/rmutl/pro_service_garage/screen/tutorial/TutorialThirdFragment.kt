package th.ac.rmutl.pro_service_garage.screen.tutorial

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.screen.login.LoginActivity
import th.ac.rmutl.pro_service_garage.screen.register.RegisterProfileActivity
/**
 * A simple [Fragment] subclass.
 */
class TutorialThirdFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater!!.inflate(R.layout.fragment_tutorial_third, container, false)
        val buttonRegister = rootView.findViewById<TextView>(R.id.button_register)
        val buttonLogin = rootView.findViewById<TextView>(R.id.button_login)
        buttonRegister.setOnClickListener{
            goToRegisterActivity()
        }

        buttonLogin.setOnClickListener{
            goToLoginActivity()
        }
        return rootView
    }

    fun goToRegisterActivity(){
        var intent = Intent(context, RegisterProfileActivity::class.java)
        startActivity(intent)
    }

    fun goToLoginActivity(){
        var intent = Intent(context, LoginActivity::class.java)
        startActivity(intent)
    }

}// Required empty public constructor
package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/12/18.
 */

data class EmptyResponse(
        override val res_code: String,
        override val res_desc: String,
        override val token: String
) : Response
package th.ac.rmutl.pro_service_garage.screen.tutorial

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import th.ac.rmutl.pro_service_garage.screen.tutorial.TutorialFirstFragment
import th.ac.rmutl.pro_service_garage.screen.tutorial.TutorialThirdFragment
import th.ac.rmutl.pro_service_garage.model.Banner


/**
 * Created by digio-macair13 on 2/23/18.
 */
class FragmentViewPagerAdapter(fragmentManager: FragmentManager?,banner: ArrayList<Banner>) : FragmentPagerAdapter(fragmentManager) {

    var fragmentManager: FragmentManager? = null
    var banner : ArrayList<Banner>? = null

    init {
        this.fragmentManager = fragmentManager
        this.banner = banner
    }

    override fun getItem(position: Int): Fragment? {
        for (i in 0..banner!!.size - 1 ) {
            if (position == i){
                val bannerFirst: Banner = banner!!.get(i)
                return TutorialFirstFragment.newInstance(bannerFirst.title,bannerFirst.message,bannerFirst.image)
            }
        }
        if(banner!!.size == position){
            return TutorialThirdFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return banner!!.size + 1
    }

}
package th.ac.rmutl.pro_service_garage.screen.main


import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.screen.repair_detail.RepairDetailActivity
import th.ac.rmutl.pro_service_garage.model.*
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus


/**
 * A simple [Fragment] subclass.
 */
class HistoryFragment : Fragment(), HistoryListAdapter.HistoryClick{

    private var progressDialog: ProgressDialog? = null
    var historyRecycler: RecyclerView? = null

    override fun onClick(repair: Repair) {
        goToRepairDetailActivity(repair)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater!!.inflate(R.layout.fragment_history, container, false)
        historyRecycler = root.findViewById<RecyclerView>(R.id.history_recycler_view)
        historyRecycler!!.setHasFixedSize(true)
        val mLayoutManager  = LinearLayoutManager(activity)
        historyRecycler!!.setLayoutManager(mLayoutManager)

        val accountPreference = AccountPreferences(context!!)
        val userId = accountPreference.getUserId().toInt()

        progressDialog = ProgressDialog(context)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)
            val api = Api()
            progressDialog?.show()
            api.historyRequest(UserRequest(userId))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ historyResponse -> checkResponse(historyResponse) },
                            { e -> println(e) },
                            {      println("onComplete") })

        return root
    }

    fun goToRepairDetailActivity(repair: Repair){
        var intent = Intent(context, RepairDetailActivity::class.java)
        intent.putExtra("repair", repair)
        startActivity(intent)
    }


    fun toastMessage(message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(historyResponse : HistoryResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(historyResponse.res_code)){
            val historyListAdapter = HistoryListAdapter(historyResponse.list_history, this)
            historyRecycler!!.setAdapter(historyListAdapter)
        }else{
            toastMessage(historyResponse.res_desc)
        }
    }
}

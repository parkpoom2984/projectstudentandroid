package th.ac.rmutl.pro_service_garage.screen.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header_navigation_view.view.*
import th.ac.rmutl.pro_service_garage.screen.tutorial.TutorialActivity
import android.widget.Toast
import android.content.pm.PackageManager


class MainActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener{
    private val REQUEST_PERMISSION_FINE_LOCATION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigationView.setNavigationItemSelectedListener(this)

        setFirstFragment()

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, 0, 0)
        drawer_layout.setDrawerListener(toggle)
        toggle.syncState()

        val accountPreference = AccountPreferences(this)
        val firstName = accountPreference.getFirstName()
        val lastName = accountPreference.getLastName()
        val phoneNo = accountPreference.getPhoneNo()
        val header :View = navigationView.inflateHeaderView(R.layout.header_navigation_view)
        header.name.setText(firstName + " "+ lastName)
        header.phoneNo.setText(phoneNo)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id :Int = item.itemId
        var fragment: Fragment? = null

        if(id == R.id.nav_history){
            setAppBarPrimary()
            toolbar.setTitle("ประวัติการซ่อม")
            fragment = HistoryFragment()
        }else if(id == R.id.nav_schedule){
            setAppBarPrimary()
            toolbar.setTitle("ตารางนัดหมาย")
            fragment = ScheduleFragment()
        }else if(id == R.id.nav_repair){
            setAppBarPrimary()
            toolbar.setTitle("แจ้งซ่อม")
            fragment = RepairFragment()
        }else if(id == R.id.nav_term_and_con){
            setAppBarPrimary()
            toolbar.setTitle("เงื่อนไขใช้บริการ")
            fragment = TermAndConditionFragment()
        }else if(id == R.id.nav_profile){
            setAppBarTransparent()
            toolbar.setTitle("โปรไพล์")
            fragment = ProfileFragment()
        }else if(id == R.id.nav_logout){
            setAppBarPrimary()
            val accountPreference = AccountPreferences(this)
            accountPreference.delete()
            finish()
            goToTutorialActivity()
        }

        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            if(fragment is RepairFragment){
                ft.replace(R.id.frame_container, fragment, "repairFragment")
            }else{
                ft.replace(R.id.frame_container, fragment)
            }
            ft.commit()
        }
        drawer_layout.closeDrawer(Gravity.START)
        return true
    }


    fun goToTutorialActivity(){
        var intent = Intent(this, TutorialActivity::class.java)
        startActivity(intent)
    }

    fun setFirstFragment(){
        navigationView.getMenu().getItem(0).setChecked(true);
        toolbar.setTitle("แจ้งซ่อม")
        setAppBarPrimary()
        var fragment: Fragment?  = RepairFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frame_container, fragment, "repairFragment")
        ft.commit()
    }

    fun setAppBarTransparent(){
        appbar.setBackgroundColor(resources.getColor(android.R.color.transparent))
    }

    fun setAppBarPrimary(){
        appbar.setBackgroundColor(resources.getColor(R.color.psg_primary))
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION_FINE_LOCATION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Permission Granted!", Toast.LENGTH_SHORT).show()
                val repairFragment = supportFragmentManager.findFragmentByTag("repairFragment") as RepairFragment
                repairFragment.setupLocation()
            } else {
                Toast.makeText(this@MainActivity, "Permission Denied!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

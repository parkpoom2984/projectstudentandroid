package th.ac.rmutl.pro_service_garage.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import th.ac.rmutl.pro_service_garage.model.*

/**
 * Created by digio-macair13 on 2/12/18.
 */
class Api {
    var service : Service

    init {
        val httpClient = OkHttpClient().newBuilder()
        val interceptor = Interceptor { chain ->
            var original = chain.request()
            val request = chain?.request()?.newBuilder()?.header("Authorization", "Basic cGFya3Bvb206MXFhelpBUSE=")?.method(original.method(), original.body())?.build();
            chain?.proceed(request)
        }

        var logging = HttpLoggingInterceptor()
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        httpClient.networkInterceptors().add(interceptor)
// add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        var client = httpClient.build()

        var retrofit = Retrofit.Builder()
                .baseUrl("https://proservicegarage.azurewebsites.net/").addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).client(client).build()

        service = retrofit.create(Service::class.java)
    }


    fun loginRequest(loginRequest : LoginRequest) : Observable<Account> {
        return service.loginRequest(loginRequest)
    }

    fun registerRequest(registerRequest: RegisterRequest) : Observable<Account> {
        return service.registerRequest(registerRequest)
    }

    fun getBannerRequest() : Observable<BannerResponse> {
        return service.getBannerRequest()
    }

    fun registerAddressRequest(addressRequest: AddressRequest) : Observable<EmptyResponse> {
        return service.registerAddressRequest(addressRequest)
    }

    fun repairRequest(repairRequest: RepairRequest) : Observable<EmptyResponse> {
        return service.repairRequest(repairRequest)
    }

    fun historyRequest(userRequest: UserRequest) : Observable<HistoryResponse> {
        return service.historyRequest(userRequest)
    }

    fun scheduleRequest(userRequest: UserRequest) : Observable<ScheduleResponse> {
        return service.scheduleRequest(userRequest)
    }

    fun feedbackRequest(feedbackRequest: FeedbackRequest) : Observable<EmptyResponse> {
        return service.feedBackRequest(feedbackRequest)
    }

    fun addCarRequest(addCarRequest: AddCarRequest) : Observable<EmptyResponse> {
        return service.addCarRequest(addCarRequest)
    }

    fun listCarRequest(userRequest: UserRequest) : Observable<ListCarResponse> {
        return service.listCarRequest(userRequest)
    }

    fun employeeActiveRequest(employeeActiveRequest: EmployeeActiveRequest) : Observable<EmptyResponse> {
        return service.employeeActiveRequest(employeeActiveRequest)
    }

}
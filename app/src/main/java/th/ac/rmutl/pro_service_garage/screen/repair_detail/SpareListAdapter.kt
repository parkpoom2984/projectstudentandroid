package th.ac.rmutl.pro_service_garage.screen.repair_detail

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.Spare

class SpareListAdapter(private val listSpare: ArrayList<Spare>) : RecyclerView.Adapter<SpareListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val card_view = LayoutInflater.from(parent.context).inflate(R.layout.card_spare, null)
        val viewHolder = ViewHolder(card_view)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val spare = listSpare[position]
        holder!!.spareNameTextView.setText(spare.spareName)
        holder!!.spareTotalTextView.setText(spare.total.toString())
        var price = spare.total * spare.price
        holder!!.sparePriceTextView.setText(price.toString())
    }

    override fun getItemCount(): Int {
        return listSpare.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var spareNameTextView = itemView.findViewById<TextView>(R.id.spare_name)
        var spareTotalTextView = itemView.findViewById<TextView>(R.id.spare_total)
        var sparePriceTextView = itemView.findViewById<TextView>(R.id.spare_price)
    }
}
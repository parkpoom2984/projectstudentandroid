package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 4/28/18.
 */
data class EmployeeActiveRequest(
        val repairId: Int
)
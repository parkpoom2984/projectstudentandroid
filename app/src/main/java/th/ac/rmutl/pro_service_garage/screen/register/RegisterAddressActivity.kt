package th.ac.rmutl.pro_service_garage.screen.register

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import th.ac.rmutl.pro_service_garage.R
import kotlinx.android.synthetic.main.activity_register_address.*
import th.ac.rmutl.pro_service_garage.model.AddressRequest
import th.ac.rmutl.pro_service_garage.model.EmptyResponse
import th.ac.rmutl.pro_service_garage.prefer.AccountPreferences
import th.ac.rmutl.pro_service_garage.service.Api
import th.ac.rmutl.pro_service_garage.utils.ApiStatus

class RegisterAddressActivity : AppCompatActivity() {

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_address)
        setSupportActionBar(toolbar)
        val accountPreference = AccountPreferences(this)
        val userId = accountPreference.getUserId().toInt()

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("โหลดข้อมูล")
        progressDialog?.setCancelable(false)
        progressDialog?.setCanceledOnTouchOutside(false)

        button_save.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val api = Api()
                progressDialog?.show()
                api.registerAddressRequest(AddressRequest(userId,input_number.text.toString(), input_subDistrict.text.toString(), input_district.text.toString(), input_province.text.toString(), input_zipCode.text.toString()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response -> checkResponse(response) },
                                { e -> println(e) },
                                {      println("onComplete") })
            }
        })
    }

    fun goToRegisterSuccessActivity(){
        val intent = Intent(this, RegisterSuccessActivity::class.java)
        startActivity(intent)
        finish()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.register_address_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_skip -> {
                // Action goes here
                goToRegisterSuccessActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun toastMessage(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun checkResponse(response : EmptyResponse){
        progressDialog?.hide()
        if(ApiStatus().isSuccess(response.res_code)){
            val address =  input_number.text.toString() + " " + input_subDistrict.text.toString() + " " +  input_district.text.toString() + " " +  input_province.text.toString() + " " +  input_zipCode.text.toString()
            val accountPreference = AccountPreferences(this)
            accountPreference!!.setAddress(address)
            goToRegisterSuccessActivity()
        }else{
            toastMessage(response.res_desc)
        }
    }

}

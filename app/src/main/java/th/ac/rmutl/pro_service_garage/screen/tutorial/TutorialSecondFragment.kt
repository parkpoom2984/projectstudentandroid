package th.ac.rmutl.pro_service_garage.screen.tutorial

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.ac.rmutl.pro_service_garage.R

/**
 * A simple [Fragment] subclass.
 */
class TutorialSecondFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val title = arguments?.getString("title")
        val message = arguments?.getString("message")
        val image = arguments?.getString("image")

        return inflater!!.inflate(R.layout.fragment_tutorial_second, container, false)
    }

    companion object {
        fun newInstance(title: String, message: String, image: String): TutorialFirstFragment {
            val args = Bundle()
            args.putString("title", title)
            args.putString("message", message)
            args.putString("image", image)
            val fragment = TutorialFirstFragment()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
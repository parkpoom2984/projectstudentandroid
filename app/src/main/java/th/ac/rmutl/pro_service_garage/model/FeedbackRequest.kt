package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class FeedbackRequest(
        val repairId: Int,
        val rate: Int,
        val message: String
)
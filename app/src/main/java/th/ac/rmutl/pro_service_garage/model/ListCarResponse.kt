package th.ac.rmutl.pro_service_garage.model

/**
 * Created by digio-macair13 on 2/21/18.
 */

data class ListCarResponse(
        val list_car: ArrayList<Car>,
        override val res_code: String,
        override val res_desc: String,
        override val token: String
): Response
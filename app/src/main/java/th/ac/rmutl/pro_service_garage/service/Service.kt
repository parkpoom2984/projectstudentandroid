package th.ac.rmutl.pro_service_garage.service

import io.reactivex.Observable
import retrofit2.http.*
import th.ac.rmutl.pro_service_garage.model.*

/**
 * Created by digio-macair13 on 2/12/18.
 */
interface Service {
    @POST("api/login")
    fun loginRequest(@Body loginRequest: LoginRequest) : Observable<Account>

    @POST("api/register")
    fun registerRequest(@Body registerRequest: RegisterRequest) : Observable<Account>

    @POST("api/banner")
    fun getBannerRequest() : Observable<BannerResponse>

    @POST("api/save_address")
    fun registerAddressRequest(@Body addressRequest: AddressRequest) : Observable<EmptyResponse>

    @POST("api/create_repair")
    fun repairRequest(@Body repairRequest: RepairRequest) : Observable<EmptyResponse>

    @POST("api/history")
    fun historyRequest(@Body userRequest: UserRequest) : Observable<HistoryResponse>

    @POST("api/schedule")
    fun scheduleRequest(@Body userRequest: UserRequest) : Observable<ScheduleResponse>

    @POST("api/save_feedback")
    fun feedBackRequest(@Body feedbackRequest: FeedbackRequest) : Observable<EmptyResponse>

    @POST("api/create_car")
    fun addCarRequest(@Body addCarRequest: AddCarRequest) : Observable<EmptyResponse>

    @POST("api/list_car")
    fun listCarRequest(@Body userRequest: UserRequest) : Observable<ListCarResponse>

    @POST("api/update_employee_active")
    fun employeeActiveRequest(@Body employeeActiveRequest: EmployeeActiveRequest) : Observable<EmptyResponse>

}
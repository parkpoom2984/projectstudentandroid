package th.ac.rmutl.pro_service_garage.screen.main

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.ac.rmutl.pro_service_garage.R
import th.ac.rmutl.pro_service_garage.model.Schedule
import java.text.SimpleDateFormat



class ScheduleListAdapter(private val listSchedule: ArrayList<Schedule>) : RecyclerView.Adapter<ScheduleListAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val card_view = LayoutInflater.from(parent.context).inflate(R.layout.card_schedule, null)
        val viewHolder = ViewHolder(card_view)
        //card_view.setOnClickListener(this)
        //viewHolder.cardView!!.setOnClickListener(this)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val quotation = listSchedule[position]
        val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(quotation.pickDate.replace("Z$".toRegex(), "+0000"))
        holder!!.dateTextView.setText(SimpleDateFormat("dd-MM-yyyy HH:mm").format(date))
        holder!!.carTextView.setText(quotation.licensePlate + " " + quotation.provincePlate)
    }

    override fun getItemCount(): Int {
        return listSchedule.size
    }

    override fun onClick(view: View) {
//        if (view.id == R.id.card_view) {
//            listener.onClick(view.tag as Locations)
//        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dateTextView = itemView.findViewById<TextView>(R.id.date_textview)
        var carTextView = itemView.findViewById<TextView>(R.id.car_textview)
        var cardView = itemView.findViewById<CardView>(R.id.card_view)
    }

    interface HistoryClick {
        fun onClick(location: String)
    }
}